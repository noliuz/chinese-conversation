﻿using UnityEngine;
using System.Collections;
using System;

public class main : MonoBehaviour
{      

  Animator c_anim;
  Animator t_anim;
  SpriteRenderer t_ball;
  SpriteRenderer c_ball;
  AudioSource t_sound;
  AudioSource c_sound;
  float t;
  // Use this for initialization
  void Start()
  {
    Screen.SetResolution(800, 600, true);
        
    /*
        if (Application.loadedLevelName == "main")
          {
            t =Time.time;
            return;
          }
          */
    c_anim = GameObject.Find("chinese_girl").GetComponent<Animator>();
    t_anim = GameObject.Find("thai_girl").GetComponent<Animator>();
    t_ball = GameObject.Find("t1").GetComponent<SpriteRenderer>();
    c_ball = GameObject.Find("c1").GetComponent<SpriteRenderer>();
    t_sound = GameObject.Find("tsound1").GetComponent<AudioSource>();
    c_sound = GameObject.Find("csound1").GetComponent<AudioSource>();

    t_ball.renderer.enabled = false;
    c_ball.renderer.enabled = false;
    t_sound.Stop();
    c_sound.Stop();

    t = Time.time;

  }

  int state = 0;
  // Update is called once per frame
  void Update()
  {
    /*
        if (Application.loadedLevelName == "main")
        {
            if (Time.time - t > 1) {
                Application.LoadLevel("a");
                t = Time.time;
            }
        }
          */  
        
    switch (state)
      {
      case 0: 
        c_ball.renderer.enabled = true; 
        state = 1;
        t = Time.time;
        break;
      case 1:
        if (Time.time - t > 0.5)
          {
            c_sound.Play();
            c_anim.Play("c_say_motion");
            state = 2;
          }
          break;
        case 2:
          if (c_sound.isPlaying)
            break;
          c_anim.Play("c_idle");
          state = 3;
          t = Time.time;
          break;
        case 3:
          if (Time.time - t > 0.5)
            {
              state = 4;
              break;
            }
            break;
          case 4:
            t_ball.renderer.enabled = true; 
            state = 5;
            t = Time.time;
            break;
          case 5:
            if (Time.time - t > 0.5)
              {
                t_sound.Play();
                t_anim.Play("t_say_motion");
                state = 6;
              }
              break;
            case 6:
              if (t_sound.isPlaying)
                break;
              t_anim.Play("t_idle");
              state = 7;
              t = Time.time;
              break;
            case 7: 
                //String txt = EditorApplication.currentScene;
              String txt = Application.loadedLevelName;
                /*
                String[] at = txt.Split('/');
                String[] at2 = at[1].Split('.');
                */
              String sc = txt;
              Debug.Log(txt);

              if (sc == "a")
                Application.LoadLevel("b");
              else if (sc == "b")
                Application.LoadLevel("c");
              else if (sc == "c")
                Application.LoadLevel("d");
              else if (sc == "d")
                Application.LoadLevel("e");
              else if (sc == "e")
                Application.LoadLevel("f");
              else if (sc == "f")
                Application.LoadLevel("g");
              else if (sc == "g")
                Application.LoadLevel("h");
              else if (sc == "h")
                Application.LoadLevel("i");
              else if (sc == "i")
                Application.LoadLevel("j");
              else if (sc == "j")
                Application.LoadLevel("k");
              else if (sc == "k")
                Application.Quit();

              break;

      }

  }
}
